/*
 * Copyright by the original author or authors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.ccc.events.badge.card10.filetransfer

import android.util.Log
import de.ccc.events.badge.card10.filetransfer.protocol.Packet
import de.ccc.events.badge.card10.filetransfer.protocol.PacketType
import de.ccc.events.badge.card10.filetransfer.protocol.TransferState
import java.nio.ByteBuffer
import java.nio.charset.Charset

private const val TAG = "FileTransfer"

class FileTransfer(
    private val service: LowEffortService,
    private val reader: ChunkedReader,
    private var listener: FileTransferListener,
    private val destinationPath: String
) : OnPacketReceivedListener {

    private var currentState = TransferState.IDLE

    init {
        service.setOnPacketReceivedListener(this)
    }

    override fun onPacketReceived(packet: Packet) {
        when (packet.type) {
            PacketType.START_ACK -> {
                if (currentState != TransferState.START_SENT) {
                    abort()
                } else {
                    currentState = TransferState.READY_TO_SEND
                    sendNext()
                }
            }

            PacketType.CHUNK_ACK -> {
                if (currentState == TransferState.READY_TO_SEND || currentState == TransferState.CHUNK_SENT) {
                    if (verifyCrc(packet)) {
                        sendNext()
                    } else {
                        // TODO: Retry
                        abort()
                    }
                } else {
                    abort()
                }
            }

            PacketType.FINISH_ACK -> {
                if (currentState != TransferState.FINISH_SENT) {
                    abort()
                } else {
                    currentState = TransferState.IDLE
                    listener.onFinish()
                }
            }

            PacketType.ERROR -> {
                // Abort transfer
                peripheralAbort()
                listener.onError()
            }

            PacketType.ERROR_ACK -> {
                // TODO
            }

            else -> {
                abort()
            }
        }
    }

    fun start() {
        if (currentState != TransferState.IDLE) {
            throw IllegalStateException()
        }

        currentState = TransferState.START_SENT
        service.sendPacket(
            Packet(
                PacketType.START,
                destinationPath.toByteArray(Charset.forName("ASCII"))
            )
        )
    }

    private fun sendNext() {
        if (!reader.isDone()) {
            var status = service.sendPacket(reader.getNext())
            var retryCount = 1
            while (!status && retryCount < 3) {
                status = service.sendPacket(reader.getLast())
                retryCount++
            }
        } else {
            service.sendPacket(
                Packet(
                    PacketType.FINISH,
                    ByteArray(0)
                )
            )
            currentState = TransferState.FINISH_SENT
        }
    }

    private fun verifyCrc(packet: Packet): Boolean {
        val crcBuf = ByteBuffer.wrap(packet.payload)
        val buf = ByteBuffer.allocate(8)
        buf.putInt(0)
        buf.put(crcBuf)
        val long = buf.getLong(0)
        return reader.verifyCrc(long)
    }

    private fun peripheralAbort() {
        // Peripheral sent error. Only clean up
    }

    fun abort() {
        // We want to abort. Clean up and send ERROR
        service.sendPacket(
            Packet(
                PacketType.ERROR,
                ByteArray(0)
            )
        )
        listener.onError()
    }
}