# Android Companion App

[![pipeline status](https://git.card10.badge.events.ccc.de/card10/companion-app-android/badges/master/pipeline.svg)](https://git.card10.badge.events.ccc.de/card10/companion-app-android/pipelines)

## Download

Per F-Droid nightly repository:

[![https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/icon.png)](https://git.card10.badge.events.ccc.de/card10/companion-app-android-nightly/raw/master/fdroid/repo)
